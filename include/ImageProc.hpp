#pragma once
#include "opencv2/opencv.hpp"
#include <utility>
#include <string>
#include <cmath>
#include <cstdlib>
#include <numeric>

#define PRODUCTION

#ifdef PRODUCTION
#include "ros/ros.h"
#include "ROSManager.hpp"
#endif

/*
 * ImageProc содержит в себе параметры распознавания, ответственен за то, чтобы по картинке или последовательности кадров найти, где вспышки
 * и указать их на кадре.
 */

class ImgProc {

public:
    typedef struct {
        cv::Point2d center;
        double radius;
        double mean_brightness;
        double proba;
    } Flash;

    typedef std::pair<cv::Mat, Flash> Picture;

    typedef std::vector<bool> Sequence;

private:

    std::vector<Picture> Seq;

    bool lazy;

public:

    ImgProc(bool lazy=false);

    virtual Flash DetectFlash(cv::Mat img);
    
    Picture FromImage(cv::Mat);
    
    Sequence ToSequence(void);

    int Add(cv::Mat img);

    static void ShowPicture(std::string, Picture &p);

    static cv::Mat ConvertTo(cv::Mat img);

    static bool check(cv::Mat img);   
};


class FlashDetector : public ImgProc {
    
public:
    Flash DetectFlash(cv::Mat);
    double compare_mse(cv::InputArray src1, cv::InputArray src2);
    cv::Scalar compare_ssim(cv::InputArray src1, cv::InputArray src2);
    void convert(cv::InputArray input, cv::OutputArray output, int dtype);
    void gaussian_gradient_magnitude(
                                    cv::InputArray   input,
                                    double           sigma,
                                    cv::OutputArray  output,
                                    cv::BorderTypes  mode = cv::BORDER_DEFAULT
                                );
    void maximize_gradient(
                            cv::InputArray     input, 
                            cv::OutputArray    output,
                            double             start,     
                            double             end,     
                            size_t             num
                        );
};


#ifdef PRODUCTION

template< typename T, typename U = typename std::enable_if<std::is_base_of<ImgProc, T>::value>::type >
class ROSImage : public ROSManager {
    T *prc;
//    sensor_msgs::Image // TODO 
public:
    ROSImage(int argc, char** argv, std::string name);

    static ImgProc::Flash ToFlash(std_msgs::Float64MultiArray &msg);
    static std_msgs::Float64MultiArray FromFlash(ImgProc::Flash fl);

    virtual void MainLoop(void);
};

#endif
