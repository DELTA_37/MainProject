#include <iostream>
#include <vector>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Byte.h"
#include "std_msgs/ByteMultiArray.h"
#include "std_msgs/Char.h"
#include "std_msgs/ColorRGBA.h"
#include "std_msgs/Duration.h"
#include "std_msgs/Empty.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Header.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Int16MultiArray.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int32MultiArray.h"
#include "std_msgs/Int64.h"
#include "std_msgs/Int64MultiArray.h"
#include "std_msgs/Int8.h"
#include "std_msgs/Int8MultiArray.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/String.h"
#include "std_msgs/Time.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/UInt16MultiArray.h"
#include "std_msgs/UInt32.h"
#include "std_msgs/UInt32MultiArray.h"
#include "std_msgs/UInt64.h"
#include "std_msgs/UInt64MultiArray.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/UInt8MultiArray.h"

#define N 1000

class ROSManager
{
    std::string nodename;
    ros::NodeHandle                            node;
    std::map<std::string, ros::Publisher>       pub;
    std::map<std::string, ros::Subscriber>      sub;
public:
    ROSManager(int argc, char**argv, std::string name);

    template<typename T>                                                            // добавить типизацию, этот шаблон один из std_msgs::*
    void AddPublisher(std::string name);

    template<typename T, typename MsgT>                                                            // добавить типизацию, этот шаблон один из std_msgs::*
    void AddSubscriber(std::string name, void (*)(MsgT&));

    virtual void MainLoop(void);
};
