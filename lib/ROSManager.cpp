#include "ROSManager.hpp"


//------------------------ROSManager------------------------------//

ROSManager::ROSManager(int argc, char **argv, std::string nodename) {
    ros::init(argc, argv, nodename);
    this->nodename = nodename;
}

template<typename T, typename MsgT> // T in std_msgs::, MsgT in std_msgs::T::ConstPtr
void ROSManager::AddSubscriber(std::string name, void (*func)(MsgT& msg))
{
    this->sub.emplace(name, this->node.subscribe<T>(name.c_str(), N, func));
}

template<typename T>  // inside std_msgs::
void ROSManager::AddPublisher(std::string name)
{
    this->pub.emplace(name, this->node.advertise<T>(name.c_str(), N));
}

void ROSManager::MainLoop(void)
{
    while(ros::ok())
    {
        
    }
}
