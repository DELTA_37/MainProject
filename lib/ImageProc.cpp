#include "ImageProc.hpp"

//------------------------ImgProc---------------------------//

ImgProc::Picture ImgProc::FromImage(cv::Mat img)
{
    ImgProc::Flash fl = DetectFlash(img);

    return std::make_pair(img, fl);
}

void ImgProc::ShowPicture(std::string name, ImgProc::Picture &p)
{
    cv::Mat img = p.first;
    ImgProc::Flash fl = p.second;
    
    cv::circle(img, fl.center, fl.radius, cv::Scalar(255, 0, 0));
    
    cv::imshow(name.c_str(), img);
}

ImgProc::Sequence ImgProc::ToSequence(void)
{
    ImgProc::Sequence s(this->Seq.size());
    for (std::size_t i = 0; i < this->Seq.size(); i++)
    {
        s[i] = (bool)(this->Seq[i].second.proba > 0.5);
    }
    return s;
}

ImgProc::Flash ImgProc::DetectFlash(cv::Mat img)
{
    
}

ImgProc::ImgProc(bool _lazy) : Seq(0), lazy(_lazy)
{
    
}

bool ImgProc::check(cv::Mat img)
{
    return true;
}

cv::Mat ImgProc::ConvertTo(cv::Mat img)
{
    return img;
}


//----------------------------FlashDetector-------------------------------//

FlashDetector::Flash FlashDetector::DetectFlash(cv::Mat img)
{
// Insert your code here...   :) 
}

double FlashDetector::compare_mse(cv::InputArray src1, cv::InputArray src2) {
    cv::Mat img1, img2;
    convert(src1.getMat(), img1, CV_64F);
    convert(src2.getMat(), img2, CV_64F);
    return pow(10, -PSNR(img1, img2) / 10);
}

cv::Scalar FlashDetector::compare_ssim(cv::InputArray src1, cv::InputArray src2) {
    double C1 = 0.0001, C2 = 0.0009, sigma = 1.5;
    int ksize = lround(fma(ceil(3 * sigma), 2, 1));
    
    cv::Mat img1, img2;
    convert(src1.getMat(), img1, CV_64F);
    convert(src2.getMat(), img2, CV_64F);
    cv::Mat img1_2     = img1 * img1;
    cv::Mat img2_2     = img2 * img2;
    cv::Mat img1_img2  = img1 * img2;
    
    cv::Mat mu1, mu2;
    cv::GaussianBlur(img1, mu1, cv::Size(ksize, ksize), sigma);
    cv::GaussianBlur(img2, mu2, cv::Size(ksize, ksize), sigma);
    cv::Mat mu1_2      = mu1 * mu1;
    cv::Mat mu2_2      = mu2 * mu2;
    cv::Mat mu1_mu2    = mu1 * mu2;
    
    cv::Mat std1_2, std2_2, std1_std2;
    cv::GaussianBlur(img1_2   , std1_2   , cv::Size(ksize, ksize), sigma);
    cv::GaussianBlur(img2_2   , std2_2   , cv::Size(ksize, ksize), sigma);
    cv::GaussianBlur(img1_img2, std1_std2, cv::Size(ksize, ksize), sigma);
    std1_2     -= mu1_2;
    std2_2     -= mu2_2;
    std1_std2  -= mu1_mu2;
    
    cv::Mat ssim_map = (2 * mu1_mu2 + C1) * (2 * std1_std2 + C2) / ((mu1_2 + mu2_2 + C1) * (std1_2 + std2_2 + C2));
    cv::Scalar mssim = cv::mean(ssim_map);
    return mssim;
}

void FlashDetector::convert(cv::InputArray input, cv::OutputArray output, int dtype) {
    cv::Mat image = input.getMat();
    
    double dst_max, dst_min;
    switch (dtype) {
        case CV_8S:
            dst_min = SCHAR_MIN;
            dst_max = SCHAR_MAX;
            break;
            
        case CV_8U:
            dst_min = 0;
            dst_max = UCHAR_MAX;
            break;
            
        case CV_16S:
            dst_min = SHRT_MIN;
            dst_max = SHRT_MAX;
            break;
            
        case CV_16U:
            dst_min = 0;
            dst_max = USHRT_MAX;
            break;
            
        case CV_32S:
            dst_min = INT_MIN;
            dst_max = INT_MAX;
            break;
            
        default:
            dst_min = 0;
            dst_max = 1;
            break;
    }
    
    double src_max, src_min;
    switch (image.depth()) {
        case CV_8S:
            src_min = SCHAR_MIN;
            src_max = SCHAR_MAX;
            break;
            
        case CV_8U:
            src_min = 0;
            src_max = UCHAR_MAX;
            break;
            
        case CV_16S:
            src_min = SHRT_MIN;
            src_max = SHRT_MAX;
            break;
            
        case CV_16U:
            src_min = 0;
            src_max = USHRT_MAX;
            break;
            
        case CV_32S:
            src_min = INT_MIN;
            src_max = INT_MAX;
            break;
            
        default:
            src_min = 0;
            src_max = 1;
            break;
    }
    
    cv::Mat m;
    double alpha = (dst_max - dst_min) / (src_max - src_min);
    image.convertTo(m, dtype, alpha, dst_min - alpha * src_min);
    output.assign(m);
}

void FlashDetector::gaussian_gradient_magnitude(cv::InputArray   input,
                                 double           sigma,
                                 cv::OutputArray  output,
                                 cv::BorderTypes  mode) {
    cv::Mat image          = input.getMat();
    cv::Mat gauss_kernel   = cv::getGaussianKernel(lround(fma(ceil(3 * sigma), 2, 1)), sigma);
    cv::Mat deriv_kernel(gauss_kernel.size(), gauss_kernel.type());
    std::iota(deriv_kernel.begin<double>(), deriv_kernel.end<double>(), ldexp(1 - deriv_kernel.size[0], -1));
    cv::normalize(deriv_kernel.mul(gauss_kernel), deriv_kernel, 1, 0, cv::NORM_L1);
    
    cv::Mat grad_x, grad_y;
    cv::sepFilter2D(image, grad_x, CV_64F, deriv_kernel, gauss_kernel, cv::Point(-1, -1), 0, mode);
    cv::sepFilter2D(image, grad_y, CV_64F, gauss_kernel, deriv_kernel, cv::Point(-1, -1), 0, mode);
    
    cv::Mat grad(grad_x.size(), grad_x.type());
    cv::magnitude(grad_x, grad_y, grad);
    output.assign(grad);
}

void FlashDetector::maximize_gradient(cv::InputArray     input, 
                       cv::OutputArray    output,
                       double             start,     
                       double             end,     
                       size_t             num) {
    cv::Mat image      = input.getMat();
    cv::Mat grad_max   = cv::Mat::zeros(image.size(), CV_64FC1);
    cv::Mat sigmas(num, 1, grad_max.type());
    std::iota(sigmas.begin<double>(), sigmas.end<double>(), 0);
    cv::exp(log(std::pow(start, -1) * end) / num * sigmas, sigmas);
    sigmas *= start;
    
#pragma omp parallel for
    for (size_t idx = 0; idx < num; ++idx)
    {
        cv::Mat grad;
        gaussian_gradient_magnitude(image, sigmas.at<double>(idx), grad);
        cv::max(grad_max, grad, grad_max);
    }
    
    output.assign(grad_max);
}


#ifdef PRODUCTION



//----------------------------ROSImage------------------------------------//



template<typename T, typename = typename std::enable_if<std::is_base_of<ImgProc, T>::value>::type>
ROSImage<T>::ROSImage(int argc, char** argv, std::string name) : ROSManager(argc, argv, name)
{
    this->prc = new T();
    this->AddPublisher<std_msgs::Float64MultiArray>("coordinate_flash_topic");
    this->AddPublisher<std_msgs::Bool>("is_flash");
    this->AddSubscriber<>("", ROSImage<T>::
}


template<typename T, typename = typename std::enable_if<std::is_base_of<ImgProc, T>::value>::type>
void ROSImage<T>::MainLoop(void)
{
    while(ros::ok())
    {
        
    } 
}


template<typename T, typename = typename std::enable_if<std::is_base_of<ImgProc, T>::value>::type>
ImgProc::Flash ROSImage<T>::ToFlash(std_msgs::Float64MultiArray &msg)
{
    ImgProc::Flash fl;
    fl.center           = Point2d(msg.data[0], msg.data[1]);
    fl.radius           = msg.data[2];
    fl.mean_brightness  = msg.data[3];
    fl.proba            = msg.data[4];
    return fl;
}


template<typename T, typename = typename std::enable_if<std::is_base_of<ImgProc, T>::value>::type>
std_msgs::Float64MultiArray ROSImage<T>::FromFlash(ImgProc::Flash& fl)
{
    std_msgs::Float64MultiArray msg;

    msg.data = new double[5];
    msg.data[0] = fl.center[0];
    msg.data[1] = fl.center[1];
    msg.data[2] = fl.radius;
    msg.data[3] = fl.mean_brightness;
    msg.data[4] = fl.proba;

    return msg;
} 
#endif
