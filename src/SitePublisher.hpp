#include "ROSManager.hpp"

class SitePublisher : public ROSManager {
public:
    SitePublisher(int argc, char **argv, std::string name);

    void MainLoop(void);
}
