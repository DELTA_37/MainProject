#include "../include/ImageProc.hpp"

int main(int argc, char**argv){
    
    ROSImage<FlashDetector> proc(argc, argv, std::string("ImageProc"));
    proc.MainLoop(); 
    return 0;
}
